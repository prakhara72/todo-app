import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import * as Animatable from 'react-native-animatable';

export default function TodoItem({ item, pressHandler }){

    return(
        <TouchableOpacity onPress={() => pressHandler(item.key)}>
            <Animatable.Text animation="zoomInUp" style={styles.item}>{item.text}</Animatable.Text>
            {/* <Text style={styles.item}>{item.text}</Text> */}
        </TouchableOpacity>
    )
}

const styles=StyleSheet.create({
    item: {
        color:'#fff',
        padding:16,
        marginTop:16,
        borderColor:'#bbb',
        borderWidth:1,
        borderStyle:'dashed',
        borderRadius:10,
    }
})